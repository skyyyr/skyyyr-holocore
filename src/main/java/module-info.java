open module holocore {
	requires java.sql;
	requires java.management;
	requires jdk.management;
	
	requires org.jetbrains.annotations;
	requires org.mongodb.driver.sync.client;
	requires org.mongodb.bson;
	requires org.mongodb.driver.core;
	requires me.joshlarson.jlcommon;
	
	requires com.projectswg.common;
	requires fast.json;
}
